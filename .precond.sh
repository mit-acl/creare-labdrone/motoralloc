#!/bin/bash

# This bash script is to be run in the mitacl/sfpro-board Docker image
# before catkin_make to satisfy all the necessary dependencies and
# preconditions.

# dev env workspace directory (where dependencies have been built)
WSDIR=$1
CPU=$2

# use ar from GNU bin utils
update-alternatives --install /bin/ar ar /usr/arm-oemllib32-linux-gnueabi/bin/ar 100    >/dev/null 2>&1
