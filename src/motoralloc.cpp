/**
 * @file motoralloc.h
 * @brief Optimal allocation of motor thrusts via linear program
 * @author Jesus Tordesillas <jtorde@mit.edu>
 * @author Brett Lopez <btlopez@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 20 April 2020
 */

#include "motoralloc/motoralloc.h"

namespace acl {
namespace motoralloc {

MotorAlloc::MotorAlloc(const Params& params)
: params_(params), COLS(NUM_SLACKS + params_.motors.size())
{
  //
  // Create and define the GLPK problem
  //

  lp_ = glp_create_prob();
  glp_set_prob_name(lp_, "motoralloc");
  glp_set_obj_dir(lp_, GLP_MIN);

  // initialize problem size
  glp_add_rows(lp_, ROWS);
  glp_add_cols(lp_, COLS);

  // design vars: slack vars must be positive
  glp_set_col_name(lp_, 1, "s1");
  glp_set_col_bnds(lp_, 1, GLP_LO, 0.0, 1.0);
  glp_set_col_name(lp_, 2, "s2");
  glp_set_col_bnds(lp_, 2, GLP_LO, 0.0, 1.0);
  glp_set_col_name(lp_, 3, "s3");
  glp_set_col_bnds(lp_, 3, GLP_LO, 0.0, 1.0);

  // design vars: thrust must be within actuator limits
  for (size_t i=4; i<=COLS; ++i) {
    std::stringstream ss;
    ss << "f" << i - NUM_SLACKS;

    glp_set_col_name(lp_, i, ss.str().c_str());
    glp_set_col_bnds(lp_, i, GLP_DB, params_.fmin, params_.fmax);
  }

  // set objective: z = [1 1 1 0 ... 0] * [s; f]
  glp_set_obj_coef(lp_, 1, 1);
  glp_set_obj_coef(lp_, 2, 1);
  glp_set_obj_coef(lp_, 3, 1);
  for (size_t i=4; i<=COLS; ++i) {
    glp_set_obj_coef(lp_, i, 0);
  }

  //
  // Build the model, which relies on the motor geometry
  //

  buildModel();
}

// ----------------------------------------------------------------------------

MotorAlloc::~MotorAlloc()
{
  // glp_delete_prob(lp_);
  glp_free_env();
}

// ----------------------------------------------------------------------------

void MotorAlloc::updateCoM(const Eigen::Vector3d& r_bM)
{
  params_.r_bM = r_bM;

  // rebuild the model since the geometry has changed
  buildModel();
}

// ----------------------------------------------------------------------------

double MotorAlloc::solve(const Eigen::Vector3d& F, const Eigen::Vector3d& M,
                        std::vector<double>& f, Eigen::Vector3d& slacks)
{
  // Set bounds / equality constraints using desired wrench.
  glp_set_row_bnds(lp_, 1, GLP_UP, 0.0, -F.x());
  glp_set_row_bnds(lp_, 2, GLP_UP, 0.0, -F.y());
  glp_set_row_bnds(lp_, 3, GLP_UP, 0.0, -F.z());
  glp_set_row_bnds(lp_, 4, GLP_UP, 0.0,  F.x());
  glp_set_row_bnds(lp_, 5, GLP_UP, 0.0,  F.y());
  glp_set_row_bnds(lp_, 6, GLP_UP, 0.0,  F.z());
  glp_set_row_bnds(lp_, 7, GLP_FX, M.x(), M.x());
  glp_set_row_bnds(lp_, 8, GLP_FX, M.y(), M.y());
  glp_set_row_bnds(lp_, 9, GLP_FX, M.z(), M.z());

  // load the problem data (i.e., the geometry)
  glp_load_matrix(lp_, count_, ia_, ja_, ar_);
  
  // set parameters
  glp_smcp params;
  glp_init_smcp(&params);
  params.msg_lev = GLP_MSG_ERR; // print error and warning messages only

  // solve the LP
  glp_simplex(lp_, &params);

  // recover optimal values of design variables
  slacks.x() = glp_get_col_prim(lp_, 1);
  slacks.y() = glp_get_col_prim(lp_, 2);
  slacks.z() = glp_get_col_prim(lp_, 3);
  for (size_t i=4; i<=COLS; ++i) {
    f.push_back(glp_get_col_prim(lp_, i));
  }

  // returns the objective value, z = s1 + s2 + s3
  return glp_get_obj_val(lp_);
};

// ----------------------------------------------------------------------------

void MotorAlloc::mix(const Eigen::Vector3d& F, const Eigen::Vector3d& M,
                      std::vector<double>& f)
{
  Eigen::VectorXd FM = Eigen::VectorXd::Zero(6);
  FM << F,
        M;

  // allocate control traditionally using a mixing matrix
  Eigen::VectorXd ff = Eigen::VectorXd::Zero(6);
  ff = Minv_ * FM;

  for (size_t i=0; i<ff.size(); ++i) {
    f.push_back(ff(i));
  }
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void MotorAlloc::buildModel()
{
  const size_t N = params_.motors.size(); ///< num motors

  // build model data
  ForceMap C = Eigen::MatrixXd::Zero(3, N);
  MomentMap D = Eigen::MatrixXd::Zero(3, N);
  for (size_t i=0; i<N; ++i) {
    const auto& motor = params_.motors[i];

    // calculate position of motor w.r.t com of vehicle: r_Mt = -r_bM + r_bt
    const auto r_Mt = -params_.r_bM + motor.position;

    // maps motor thrust into body frame
    C.block<3,1>(0,i) = motor.direction;

    // maps motor thrust into moments acting on center of mass
    D.block<3,1>(0,i) = r_Mt.cross(motor.direction) + params_.c * -1 * motor.spin * motor.direction;
  }

  //
  // Create the problem data
  //

  // Create data matrix, augmented with slacks: A = [-I -C; -I C; 0 D]
  ProblemData A = Eigen::MatrixXd::Zero(ROWS, COLS);

  // forces, augmented with slacks
  A.block<3,3>(0,0) = -Eigen::Matrix3d::Identity();
  A.block(0,3,3,N) = -C;
  A.block<3,3>(3,0) = -Eigen::Matrix3d::Identity();
  A.block(3,3,3,N) =  C;

  // moments, no slacks
  A.block<3,3>(6,0) = Eigen::Matrix3d::Zero();
  A.block(6,3,3,N) = D;

  //
  // Fill in the GLPK matrix
  //

  // set the problem data
  size_t k = 0; // current nz element we are considering
  for (size_t row=1; row<=ROWS; ++row) {
    for (size_t col=1; col<=COLS; ++col) {

      // GLPK uses 1-based indexing, Eigen does not.
      const size_t i = row - 1;
      const size_t j = col - 1;

      // skip zero entries
      if (A(i,j) == 0) continue;

      ++k;
      ia_[k] = row;
      ja_[k] = col;
      ar_[k] = A(i,j);
    }
  }

  // set the number of nonzero entries
  count_ = k;

  //
  // Build data for traditional pinv allocation
  //

  // build actuator effectiveness matrix
  M_ = Eigen::MatrixXd::Zero(6, N);
  M_ << C,
        D;

  // compute the pseudoinverse
  // TODO: should probably solve linear system using svd instead
  Minv_ = pseudoInverse(M_);
  flushToZero(Minv_);
};

}  // namespace motoralloc
}  // namespace acl
