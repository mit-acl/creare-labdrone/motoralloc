/**
 * @file motoralloc.h
 * @brief Optimal allocation of motor thrusts via linear program
 * @author Jesus Tordesillas <jtorde@mit.edu>
 * @author Brett Lopez <btlopez@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 20 April 2020
 */

#pragma once

#include <array>
#include <ctime>
#include <cmath>
#include <cstdint>
#include <sstream>
#include <vector>

#include <Eigen/Dense>

#include <glpk.h>

namespace acl {
namespace motoralloc {

  /**
   * @brief      Geometry of propulsion unit. Expressed in body frame.
   */
  struct Motor {
    Eigen::Vector3d position; ///< motor thrust point w.r.t body origin
    Eigen::Vector3d direction; ///< unit vector defining thrust vector
    int spin; ///< direction of prop spin: CW(-1) or CCW(1)
  };

  /**
   * @brief      Model parameters for optimal allocation
   */
  struct Params {
    double fmin = 0, fmax; ///< actuator limits [N]
    std::vector<Motor> motors; ///< motor geometry w.r.t body frame
    double c; ///< propeller drag constant [Nm/N]
    Eigen::Vector3d r_bM; ///< CoM w.r.t body origin, expressed in body frame
  };

  class MotorAlloc
  {
  public:
    MotorAlloc(const Params& params);
    ~MotorAlloc();

    /**
     * @brief      Solves the LP by finding motor thrusts f s.t. the desired
     *             moments are achieved (hard constraint) and the desired
     *             forces are achieved as closely as possible (soft constraint)
     *
     *             All vectors expressed in the body frame (i.e. flu).
     *
     * @param[in]  F       Desired forces
     * @param[in]  M       Desired moment acting on the center of mass
     * @param      f       Motor thrusts that optimally allocate motors
     * @param      slacks  Slack variables provide an indication of the
     *                     gap btwn the desired and allocated body forces
     *
     * @return     LP objective value, the sum of the slacks
     */
    double solve(const Eigen::Vector3d& F, const Eigen::Vector3d& M,
                  std::vector<double>& f, Eigen::Vector3d& slacks);

    /**
     * @brief      Allocates control via traditional mixing matrix (nonoptimal)
     *
     * @param[in]  F     Desired forces
     * @param[in]  M     Desired moment acting on the center of mass
     * @param      f     Motor thrusts that allocate motors
     */
    void mix(const Eigen::Vector3d& F, const Eigen::Vector3d& M,
                  std::vector<double>& f);

    /**
     * @brief      Updates the location of the center of mass w.r.t the body
     *
     * @param[in]  r_bM  The CoM location w.r.t the body, expressed in the body
     */
    void updateCoM(const Eigen::Vector3d& r_bM);

    /**
     * @brief      Returns the actuator effectiveness matrix that maps
     *             actuators to forces and moments on the body.
     *
     * @return     The actuator effectiveness matrix (inverse of mixing matrix)
     */
    Eigen::MatrixXd getActuatorEffectiveness() const { return M_; }

    /**
     * @brief      Returns mixing matrix for traditional allocation
     *
     * @return     The mixing matrix that maps body wrench to actuators
     */
    Eigen::MatrixXd getMixingMaxtrix() const { return Minv_; }

  private:
    Params params_;

    /// \brief Traditional allocation data
    Eigen::MatrixXd M_; ///< actuator effectiveness (f to [F; M])
    Eigen::MatrixXd Minv_; ///< mixing matrix ([F; M] to f)

    /// \brief Problem description
    static constexpr size_t NUM_SLACKS = 3; ///< one slack per force component
    static constexpr size_t ROWS = 9; ///< num of constraints (6 soft, 3 hard)
    const size_t COLS; ///< number of design variables

    /// \brief Problem data definitions
    using ForceMap = Eigen::Matrix<double, 3, Eigen::Dynamic>;
    using MomentMap = Eigen::Matrix<double, 3, Eigen::Dynamic>;
    using ProblemData = Eigen::Matrix<double, ROWS, Eigen::Dynamic>;

    /// \brief GLPK workspace variables
    glp_prob* lp_;
    static constexpr size_t SUFFICIENT_SIZE = 1000;
    int ia_[1 + SUFFICIENT_SIZE], ja_[1 + SUFFICIENT_SIZE];
    double ar_[1 + SUFFICIENT_SIZE];
    int count_; ///< num of non-zero entries used

    /**
     * @brief      Builds the problem data (A) matrix using motor geometry
     */
    void buildModel();
  };

  // https://gist.github.com/pshriwise/67c2ae78e5db3831da38390a8b2a209f

  /**
   * @brief      Calculate Moore-Penrose pseudoinverse using SVD
   *
   * @param[in]  A          Matrix to find psuedoinverse of
   * @param[in]  epsilon    Numerical tolerance for inverting singular vals
   *
   * @tparam     T          Template param for Eigen Matrix
   *
   * @return     Pseudoinverse of A
   */
  template<typename T>
  T pseudoInverse(const T &A, double epsilon = std::numeric_limits<double>::epsilon())
  {
    Eigen::JacobiSVD<T> svd(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
    const double tolerance = epsilon * std::max(A.cols(), A.rows()) * svd.singularValues().array().abs()(0);
    return svd.matrixV() * (svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0).matrix().asDiagonal() * svd.matrixU().adjoint();
  }

  template<typename T>
  void flushToZero(T& A, double threshold = 1e-10)
  {
    A = (threshold < A.array().abs()).select(A, 0.0f);
  }

}  // namespace motoralloc
}  // namespace acl
