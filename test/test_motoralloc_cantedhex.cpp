#include <iostream>
#include <memory>
#include <vector>

#include <gtest/gtest.h>

#include <Eigen/Dense>

#include "motoralloc/motoralloc.h"

class CantedHex : public ::testing::Test {
  public:
    CantedHex() {
      // initialize standard hex params and geometry (from labdrone sim yaml)
      acl::motoralloc::Params params;
      params.fmin = 0;
      params.fmax = 17.00;
      params.c = 0.0162;
      params.r_bM = Eigen::Vector3d(0.038, 0.0, 0.0);

      // for motor layout, see snap/cfg/labdrone.yaml

      // Motor 1
      acl::motoralloc::Motor m1;
      m1.position << 0.015,  0.326, 0.044;
      m1.direction << 0.342,  0.000,  0.940;
      m1.spin = -1;
      params.motors.push_back(m1);

      // Motor 2
      acl::motoralloc::Motor m2;
      m2.position << -0.290,  0.150, 0.044;
      m2.direction << -0.171, -0.296,  0.940;
      m2.spin = 1;
      params.motors.push_back(m2);

      // Motor 3
      acl::motoralloc::Motor m3;
      m3.position << -0.290, -0.150, 0.044;
      m3.direction << -0.171,  0.296,  0.940;
      m3.spin = -1;
      params.motors.push_back(m3);

      // Motor 4
      acl::motoralloc::Motor m4;
      m4.position << 0.015, -0.326, 0.044;
      m4.direction << 0.342,  0.000,  0.940;
      m4.spin = 1;
      params.motors.push_back(m4);

      // Motor 5
      acl::motoralloc::Motor m5;
      m5.position << 0.275, -0.176, 0.044;
      m5.direction << -0.171, -0.296,  0.940;
      m5.spin = -1;
      params.motors.push_back(m5);

      // Motor 6
      acl::motoralloc::Motor m6;
      m6.position << 0.275,  0.176, 0.044;
      m6.direction << -0.171,  0.296,  0.940;
      m6.spin = 1;
      params.motors.push_back(m6);

      motoralloc_.reset(new acl::motoralloc::MotorAlloc(params));
    }

  protected:
    std::unique_ptr<acl::motoralloc::MotorAlloc> motoralloc_;

};

// ----------------------------------------------------------------------------

TEST_F(CantedHex, hover_optim_ideal)
{

  Eigen::Vector3d F = Eigen::Vector3d(0, 0, 30);
  Eigen::Vector3d M = Eigen::Vector3d::Zero();
  Eigen::Vector3d slacks;
  std::vector<double> f;

  double z = motoralloc_->solve(F, M, f, slacks);

  for (size_t i=0; i<f.size(); ++i) {
    std::cout << "M" << i+1 << ": " << f[i] << std::endl;
  }

}

// ----------------------------------------------------------------------------

TEST_F(CantedHex, hover_optim)
{

  Eigen::Vector3d F = Eigen::Vector3d(-0.823589, -0.311683, 33.395964);
  Eigen::Vector3d M = Eigen::Vector3d(-0.333000, 0.061972, -0.081724);
  Eigen::Vector3d slacks;
  std::vector<double> f;

  double z = motoralloc_->solve(F, M, f, slacks);

  for (size_t i=0; i<f.size(); ++i) {
    std::cout << "M" << i+1 << ": " << f[i] << std::endl;
  }

}

// ----------------------------------------------------------------------------

TEST_F(CantedHex, extreme_optim)
{

  Eigen::Vector3d F = Eigen::Vector3d(18.045764, -20.292093, 51.261505);
  Eigen::Vector3d M = Eigen::Vector3d(1.016673, -7.094308, 0.050753);
  Eigen::Vector3d slacks;
  std::vector<double> f;

  double z = motoralloc_->solve(F, M, f, slacks);

  for (size_t i=0; i<f.size(); ++i) {
    std::cout << "M" << i+1 << ": " << f[i] << std::endl;
  }

}

// ----------------------------------------------------------------------------

TEST_F(CantedHex, hover_mix_ideal)
{

  Eigen::Vector3d F = Eigen::Vector3d(0, 0, 30);
  Eigen::Vector3d M = Eigen::Vector3d::Zero();
  std::vector<double> f;

  motoralloc_->mix(F, M, f);

  for (size_t i=0; i<f.size(); ++i) {
    std::cout << "M" << i+1 << ": " << f[i] << std::endl;
  }
}
