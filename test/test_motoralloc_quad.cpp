#include <iostream>
#include <memory>
#include <vector>

#include <gtest/gtest.h>

#include <Eigen/Dense>

#include "motoralloc/motoralloc.h"

class StandardQuad : public ::testing::Test {
  public:
    StandardQuad() {
      // initialize standard quad params and geometry (from SQ sim yaml)
      acl::motoralloc::Params params;
      params.fmin = 0;
      params.fmax = 12.95;
      params.c = 0.0571; // 0.2 / 3.5
      params.r_bM = Eigen::Vector3d::Zero(); // com is co-located with body

      /*
       * motor layout:
       * 
       *    (1) CW  | (4) CCW
       *     -------|--------
       *    (2) CCW | (3) CW
       */

      // Motor 1
      acl::motoralloc::Motor m1;
      m1.position << 0.08, 0.08, 0.0;
      m1.direction = Eigen::Vector3d::UnitZ();
      m1.spin = -1;
      params.motors.push_back(m1);

      // Motor 2
      acl::motoralloc::Motor m2;
      m2.position << -0.08, 0.08, 0.0;
      m2.direction = Eigen::Vector3d::UnitZ();
      m2.spin = 1;
      params.motors.push_back(m2);

      // Motor 3
      acl::motoralloc::Motor m3;
      m3.position << -0.08, -0.08, 0.0;
      m3.direction = Eigen::Vector3d::UnitZ();
      m3.spin = -1;
      params.motors.push_back(m3);

      // Motor 4
      acl::motoralloc::Motor m4;
      m4.position << 0.08, -0.08, 0.0;
      m4.direction = Eigen::Vector3d::UnitZ();
      m4.spin = 1;
      params.motors.push_back(m4);

      motoralloc_.reset(new acl::motoralloc::MotorAlloc(params));
    }

  protected:
    std::unique_ptr<acl::motoralloc::MotorAlloc> motoralloc_;

};

// ----------------------------------------------------------------------------

TEST_F(StandardQuad, hover)
{

  Eigen::Vector3d F = Eigen::Vector3d(0, 0, 10);
  Eigen::Vector3d M = Eigen::Vector3d::Zero();
  Eigen::Vector3d slacks;
  std::vector<double> f;

  double z = motoralloc_->solve(F, M, f, slacks);

  EXPECT_EQ(f.size(), 4);
  EXPECT_DOUBLE_EQ(f[0], 2.5);
  EXPECT_DOUBLE_EQ(f[1], 2.5);
  EXPECT_DOUBLE_EQ(f[2], 2.5);
  EXPECT_DOUBLE_EQ(f[3], 2.5);
  EXPECT_DOUBLE_EQ(z, 0);
  EXPECT_DOUBLE_EQ(slacks.x(), 0);
  EXPECT_DOUBLE_EQ(slacks.y(), 0);
  EXPECT_DOUBLE_EQ(slacks.z(), 0);
}

// ----------------------------------------------------------------------------

TEST_F(StandardQuad, hoverWithShiftedCoM)
{

  Eigen::Vector3d F = Eigen::Vector3d(0, 0, 10);
  Eigen::Vector3d M = Eigen::Vector3d::Zero();
  Eigen::Vector3d slacks;
  std::vector<double> f;

  Eigen::Vector3d r_bM = Eigen::Vector3d(0, 0.005, 0);
  motoralloc_->updateCoM(r_bM);

  double z = motoralloc_->solve(F, M, f, slacks);

  EXPECT_EQ(f.size(), 4);
  EXPECT_GT(f[0], 2.5); // with CoM shifted +y, 0,1 need to work harder
  EXPECT_GT(f[1], 2.5);
  EXPECT_LT(f[2], 2.5);
  EXPECT_LT(f[3], 2.5);
  EXPECT_DOUBLE_EQ(z, 0);
  EXPECT_DOUBLE_EQ(slacks.x(), 0);
  EXPECT_DOUBLE_EQ(slacks.y(), 0);
  EXPECT_DOUBLE_EQ(slacks.z(), 0);
}

// ----------------------------------------------------------------------------

TEST_F(StandardQuad, yaw)
{

  Eigen::Vector3d F = Eigen::Vector3d(0, 0, 10);
  Eigen::Vector3d M = Eigen::Vector3d(0, 0, 0.25);
  Eigen::Vector3d slacks;
  std::vector<double> f;

  double z = motoralloc_->solve(F, M, f, slacks);

  EXPECT_EQ(f.size(), 4);
  EXPECT_GT(f[0], f[1]); // motors 0, 2 create a positive yaw
  EXPECT_GT(f[2], f[3]);
  EXPECT_DOUBLE_EQ(z, 0);
  EXPECT_DOUBLE_EQ(slacks.x(), 0);
  EXPECT_DOUBLE_EQ(slacks.y(), 0);
  EXPECT_DOUBLE_EQ(slacks.z(), 0);
}

// ----------------------------------------------------------------------------

TEST_F(StandardQuad, fastYaw)
{

  Eigen::Vector3d F = Eigen::Vector3d(0, 0, 10);
  Eigen::Vector3d M = Eigen::Vector3d(0, 0, 1);
  Eigen::Vector3d slacks;
  std::vector<double> f;

  double z = motoralloc_->solve(F, M, f, slacks);

  EXPECT_EQ(f.size(), 4);
  EXPECT_GT(f[0], f[1]); // motors 0, 2 create a positive yaw
  EXPECT_GT(f[2], f[3]);
  EXPECT_DOUBLE_EQ(slacks.x(), 0);
  EXPECT_DOUBLE_EQ(slacks.y(), 0);
  EXPECT_DOUBLE_EQ(z, slacks.z());
  EXPECT_GT(slacks.z(), 0); // the desired force cannot be achieved while
                            // achieving desired moment
}

// ----------------------------------------------------------------------------

TEST_F(StandardQuad, OptVsMix)
{

  Eigen::Vector3d F = Eigen::Vector3d(0, 0, 10);
  Eigen::Vector3d M = Eigen::Vector3d::Zero();
  Eigen::Vector3d slacks;
  std::vector<double> fopt, fmix;

  const double z = motoralloc_->solve(F, M, fopt, slacks);
  motoralloc_->mix(F, M, fmix);

  EXPECT_EQ(fopt.size(), 4);
  EXPECT_EQ(fmix.size(), 4);
  EXPECT_DOUBLE_EQ(fopt[0], fmix[0]);
  EXPECT_DOUBLE_EQ(fopt[1], fmix[1]);
  EXPECT_DOUBLE_EQ(fopt[2], fmix[2]);
  EXPECT_DOUBLE_EQ(fopt[3], fmix[3]);
  // EXPECT_DOUBLE_EQ(z, 0);
  // EXPECT_DOUBLE_EQ(slacks.x(), 0);
  // EXPECT_DOUBLE_EQ(slacks.y(), 0);
  // EXPECT_DOUBLE_EQ(slacks.z(), 0);
}

// ----------------------------------------------------------------------------

int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
