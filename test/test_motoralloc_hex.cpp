#include <iostream>
#include <memory>
#include <vector>

#include <gtest/gtest.h>

#include <Eigen/Dense>

#include "motoralloc/motoralloc.h"

class StandardHex : public ::testing::Test {
  public:
    StandardHex() {
      // initialize standard hex params and geometry (from labdrone sim yaml)
      acl::motoralloc::Params params;
      params.fmin = 0;
      params.fmax = 17.65;
      params.c = 0.0162;
      params.r_bM = Eigen::Vector3d::Zero(); // com is co-located with body

      // for motor layout, see labdrone/doc/report.pdf

      // Motor 1
      acl::motoralloc::Motor m1;
      m1.position << 0.000,  0.326,  0.050;
      m1.direction = Eigen::Vector3d::UnitZ();
      m1.spin = -1;
      params.motors.push_back(m1);

      // Motor 2
      acl::motoralloc::Motor m2;
      m2.position << -0.282,  0.163,  0.050;
      m2.direction = Eigen::Vector3d::UnitZ();
      m2.spin = 1;
      params.motors.push_back(m2);

      // Motor 3
      acl::motoralloc::Motor m3;
      m3.position << -0.282, -0.163,  0.050;
      m3.direction = Eigen::Vector3d::UnitZ();
      m3.spin = -1;
      params.motors.push_back(m3);

      // Motor 4
      acl::motoralloc::Motor m4;
      m4.position << -0.000, -0.326,  0.050;
      m4.direction = Eigen::Vector3d::UnitZ();
      m4.spin = 1;
      params.motors.push_back(m4);

      // Motor 5
      acl::motoralloc::Motor m5;
      m5.position << 0.282, -0.163,  0.050;
      m5.direction = Eigen::Vector3d::UnitZ();
      m5.spin = -1;
      params.motors.push_back(m5);

      // Motor 6
      acl::motoralloc::Motor m6;
      m6.position << 0.282,  0.163,  0.050;
      m6.direction = Eigen::Vector3d::UnitZ();
      m6.spin = 1;
      params.motors.push_back(m6);

      motoralloc_.reset(new acl::motoralloc::MotorAlloc(params));
    }

  protected:
    std::unique_ptr<acl::motoralloc::MotorAlloc> motoralloc_;

};

// ----------------------------------------------------------------------------

// Note: this test fails because there is a null space in the actuator matrix
// and so the min-norm solution is chosen (i.e., l1 is sparse vs l2 optim).
TEST_F(StandardHex, DISABLED_hover_optim)
{

  Eigen::Vector3d F = Eigen::Vector3d(0, 0, 10);
  Eigen::Vector3d M = Eigen::Vector3d::Zero();
  Eigen::Vector3d slacks;
  std::vector<double> f;

  double z = motoralloc_->solve(F, M, f, slacks);

  EXPECT_EQ(f.size(), 6);
  EXPECT_DOUBLE_EQ(f[0], 10.0/6);
  EXPECT_DOUBLE_EQ(f[1], 10.0/6);
  EXPECT_DOUBLE_EQ(f[2], 10.0/6);
  EXPECT_DOUBLE_EQ(f[3], 10.0/6);
  EXPECT_DOUBLE_EQ(f[4], 10.0/6);
  EXPECT_DOUBLE_EQ(f[5], 10.0/6);
  EXPECT_DOUBLE_EQ(z, 0);
  EXPECT_DOUBLE_EQ(slacks.x(), 0);
  EXPECT_DOUBLE_EQ(slacks.y(), 0);
  EXPECT_DOUBLE_EQ(slacks.z(), 0);
}

// ----------------------------------------------------------------------------

TEST_F(StandardHex, hover_mix)
{

  Eigen::Vector3d F = Eigen::Vector3d(0, 0, 10);
  Eigen::Vector3d M = Eigen::Vector3d::Zero();
  std::vector<double> f;

  motoralloc_->mix(F, M, f);

  EXPECT_EQ(f.size(), 6);
  EXPECT_DOUBLE_EQ(f[0], 10.0/6);
  EXPECT_DOUBLE_EQ(f[1], 10.0/6);
  EXPECT_DOUBLE_EQ(f[2], 10.0/6);
  EXPECT_DOUBLE_EQ(f[3], 10.0/6);
  EXPECT_DOUBLE_EQ(f[4], 10.0/6);
  EXPECT_DOUBLE_EQ(f[5], 10.0/6);
}
