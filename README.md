Motor Allocation Library
========================

This ROS package uses the [GNU Linear Programming Kit](https://www.gnu.org/software/glpk/) (GLPK) to allocate motor commands for a multirotor.

## Background

Motor allocation is the process of determining the motor commands based on the desired forces and moments from the outer and inner control loops. Typically, this problem is formulated by first finding the linear maps that take motor forces and torques into body forces and torques. This requires an understanding of the motor geometry. These maps can then be inverted to extract the motor commands that should be sent to the ESCs.

In some cases, inverting this matrix is not possible or does meet all the constraints for the system. For example, attitude destabilization may occur because achieving both the desired forces and moments are weighed equally. To deal with this, we formulate motor allocation as a linear program (LP) that ensures the desired moment is achieved while relaxing the desired forces.

The optimization is formulated as
```math
\begin{aligned}
& \underset{\mathbf{f}}{\text{minimize}}
& & \|\mathbf{F} - \mathbf{B}_F\mathbf{f} \|^2 \\
& \text{subject to}
& & \mathbf{M} = \mathbf{B}_M\mathbf{f} \\
&&& 0 \le f_i \le f_\text{max} \quad \forall i
\end{aligned}
```

The backened solver [GLPK](https://www.gnu.org/software/glpk/) is installed automatically via the `CMakeLists.txt`. GLPK Reference Manual (and API documentation) is available [here](http://www.chiark.greenend.org.uk/doc/glpk-doc/glpk.pdf).

## ROS Usage

In the `CMakeLists.txt` of the package that wants to use `motoralloc`, add the library name to `find_package()`:

```cmake
find_package(catkin REQUIRED COMPONENTS motoralloc)
```

See the `tests` directory for example code.

## Details

To use this library, you must provide the geometry of the vehicle. For an example, see [`test/test_motoralloc.cpp`](test/test_motoralloc.cpp).

## Tests

To run the tests, use `catkin run_tests --no-deps motoralloc`.